package net.tncy.bookmarket.data;

public class Bookstore {
    private int id;
    private String name;
    private InventoryEntry[] inventoryEntry;


    public Bookstore() {
    }

    public Bookstore(int id, String name, InventoryEntry[] inventoryEntry) {
        this.id = id;
        this.name = name;
        this.inventoryEntry = inventoryEntry;
    }

    /** Getters and Setters */

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InventoryEntry[] getInventoryEntry() {
        return this.inventoryEntry;
    }

    public void setInventoryEntry(InventoryEntry[] inventoryEntry) {
        this.inventoryEntry = inventoryEntry;
    }

}
